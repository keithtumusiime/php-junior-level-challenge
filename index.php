<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KanzuCode Challenge</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="card card-primary">
            <div class="card-header d-flex" style="justify-content: space-between; ">
                <h3>Code Preview</h3>
                <a class="btn btn-success pull-right" data-toggle="modal" data-target=".modal">Demo</a>
            </div>
            <div class="card-body">
                <?= highlight_file('code.php') ?>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Demo Modal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form">
                <div class="form-group">
                    <label class="form-label">Select Question</label>
                    <select class="form-select" id="qtn">
                        <option value="">Select Question</option>
                        <option value="1">Question 1</option>
                        <option value="2">Question 2</option>
                    </select>
                </div>

                <div class="form-group d-none mt-3" id="opt">
                    <label class="form-label" id="label">Select Question</label>
                    <input type="text" name="val" id="answer" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary mt-2 d-none" id="submit">Submit</button>
            </form>

            <div class="mt-4 alert alert-success d-none">
            </div>
          </div>
        
        </div>
      </div>
    </div>

    <script type="text/javascript">
        var type = '';
        $('#qtn').on('change',function(e){
            $('#opt').removeClass('d-none');
            $('#opt').addClass('d-block');
            var option = this.value;
            $('#answer').val('');
            console.log(option);
            if (option == '') {
                $('#opt').addClass('d-none');
                $('#opt').removeClass('d-block');
                $('#submit').addClass('d-none');
                alert("Select a question")
            }else{ 
                $('#submit').removeClass('d-none');               
                if (option == 1) {
                    type='minmax';
                    $('#answer')[0].placeholder = "Enter numbers separated by comma(,)"
                    $('#label').html("Enter Array Elements") 
                }else{
                    type='camelcase';
                    $('#answer')[0].placeholder = "Enter string eg KanzuCodeCodingChallenge"
                    $('#label').html("CamelCase String") 
                }
            }
        });

        $('#form').on('submit',function(e){
            e.preventDefault();
            $.ajax({
                url:'code.php?type='+type,
                method:'POST',
                data: $(this).serialize(),
                success:function(resp){
                    $('.alert').removeClass('d-none');
                    $('.alert').html(resp);
                    console.log(resp);
                }
            });
        })
    </script>
</body>
</html>