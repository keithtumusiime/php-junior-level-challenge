<?php 
    /*
    *   Keith Tumusiime
    *   keithtumusiime6@gmail.com
    *   +256770786064/+256781363816
    */
    //Question 1:
    function min_max($arr){
        sort($arr);
        $size = count($arr);
        $j = $size - 1;
        $min_value = $max_value = 0;
        for ($i = 0; $i < $size - 1; $i++)
        {
            // add all elements of the array except the right most
            $min_value += $arr[$i];
            
            // add all elements of the array except the left most
            $max_value += $arr[$j];
            $j--;
        }
        return $min_value.' '.$max_value;
    }

    //Question 2:
    function camelcase($string) {
        return count(preg_split('/(?=[A-Z])/',$string));
    }



    //Handling the demo event
    if (!empty($_GET['type'])) {

        if ($_GET['type'] == 'camelcase') {
            echo "Output: ".camelcase($_POST['val']);
        }else{
            echo "Output: ".min_max(explode(",", $_POST['val']));
        }
    }    
?>